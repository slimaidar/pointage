from django.contrib import admin
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from pointage import urls

from main import views

app_name="main"
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.Home.as_view(), name="home-page"),
    path('pointage', include('pointage.urls')),

]+static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()