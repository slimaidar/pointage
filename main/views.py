from django.shortcuts import render
from django.views import View


class Home(View):
    template_name = "home-page"

    def get(self, request):
        return render(request, "home.html")