# Generated by Django 2.0.3 on 2018-03-29 12:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Absent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time_enter_morning', models.TimeField()),
                ('time_out_morning', models.TimeField()),
                ('time_enter_evening', models.TimeField()),
                ('time_out_evening', models.TimeField()),
                ('permanence', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='AtdRecord',
            fields=[
                ('SerialId', models.BigAutoField(primary_key=True, serialize=False)),
                ('RecDate', models.DateField()),
                ('RecTime', models.TimeField(max_length=255)),
                ('dateb', models.DateTimeField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='GroupSalaries',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Holidays',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('holiday_name', models.CharField(max_length=255)),
                ('start', models.DateField()),
                ('end', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Salary',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_salary_finger', models.IntegerField(unique=True)),
                ('first_name', models.CharField(max_length=255)),
                ('last_name', models.CharField(max_length=255)),
                ('picture', models.ImageField(default='img/salary/default.png', null=True, upload_to='img/salary')),
                ('group_salary', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='pointage.GroupSalaries')),
            ],
        ),
        migrations.AddField(
            model_name='atdrecord',
            name='CardNo',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='pointage.Salary'),
        ),
        migrations.AddField(
            model_name='absent',
            name='group',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='pointage.GroupSalaries'),
        ),
    ]
