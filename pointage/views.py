from django.contrib.auth import logout
from django.shortcuts import render
import datetime
from django.db.models import Max, Min
from operator import itemgetter
import math

# Create your views here.
from django.views import View

from pointage.models import Salary, GroupSalaries, AtdRecord, Absent, Holidays, Break


class Employees(View):
    template_name = "employees-home"

    def get(self, request):
        employees = Salary.objects.order_by('-id')
        return render(request, 'pointage/employees/home.html', {'employees': employees})

class Check:

    def is_holiday(self, date):
        holidays = Holidays.objects.all()
        for holiday in holidays:
            if holiday.start <= datetime.datetime.strptime(date, '%Y-%m-%d').date() <= holiday.end:
                return True
        return False

    def is_in_break(self, date, salary):
        breaks = Break.objects.filter(salary=salary)
        for brk in breaks:
            if brk.start <= datetime.datetime.strptime(date, '%Y-%m-%d').date() <= brk.end:
                return True
        return False

    def pointages(self, dates, salary):
        attendance = []
        #pointages li dar between had dates
        pointages = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(RecDate__lte=dates[-1]).filter(
            RecDate__gte=dates[0])
        attaendence_counter = {'absent': 0, 'early': 0, 'late': 0}
        exist_dates = []

        for pointage in pointages:
            if pointage.RecDate.strftime('%Y-%m-%d') in dates:
                #only and only the date li pointa fihon (likainin f DB)
                exist_dates.append(pointage.RecDate.strftime('%Y-%m-%d'))

        for i in dates:
            if i not in exist_dates:
                check = Check()
                if not check.is_holiday(i):
                    if check.is_in_break(i, salary):
                        attendance.append({'date': i, 'color': 'bg-break', 'salary': salary})  # break
                    else:
                        attaendence_counter['absent'] += 1
                        attendance.append({'date': i, 'color': 'bg-danger', 'salary': salary})  # Absent
                else:
                    attendance.append({'date': i, 'color': 'bg-blue', 'salary': salary})  # holiday

        absent = Absent.objects.filter(group=salary.group_salary.id).first()
        old_date = ''

        if absent.permanence:
            total_hours = [0, 0]
        else:
            total_hours = [0, 0, 0, 0]

        for i in pointages:
            number_of_points = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                RecDate=i.RecDate).count()

            if absent.permanence:

                min_hour = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).aggregate(Min('RecTime'))
                max_hour = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).aggregate(Max('RecTime'))

                if number_of_points < 2:
                    attendance.append(
                        {'date': i.RecDate.strftime('%Y-%m-%d'), 'color': 'bg-brown', 'enter': min_hour['RecTime__min'],
                         'out': min_hour['RecTime__min'], 'salary': salary})  # not complete
                else:
                    if i.RecDate != old_date:

                        # calculate number of working hours
                        if min_hour['RecTime__min'].hour <= absent.time_enter_morning.hour:
                            if max_hour['RecTime__max'].hour >= absent.time_out_evening.hour:
                                total_hours[0] += absent.time_out_evening.hour - absent.time_enter_morning.hour
                            else:
                                total_hours[0] += max_hour['RecTime__max'].hour - absent.time_enter_morning.hour
                                total_hours[1] += max_hour['RecTime__max'].minute

                            if min_hour['RecTime__min'].minute > absent.time_enter_morning.minute:
                                total_hours[0] -= 1
                                total_hours[1] += 60 - min_hour['RecTime__min'].minute
                        else:
                            if max_hour['RecTime__max'].hour >= absent.time_out_evening.hour:
                                total_hours[0] += absent.time_out_evening.hour - min_hour['RecTime__max'].hour - 1
                                total_hours[1] += 60 - min_hour['RecTime__max'].minute
                            else:
                                total_hours[0] += absent.time_out_evening.hour - min_hour['RecTime__max'].hour - 1
                                total_hours[1] += 60 - min_hour['RecTime__max'].minute
                        #end calculate of working hours

                        if min_hour['RecTime__min'] > absent.time_enter_morning:
                            attaendence_counter['late'] += 1
                            attendance.append({'date': i.RecDate.strftime('%Y-%m-%d'), 'color': 'bg-yellow',
                                               'enter': min_hour['RecTime__min'],
                                               'out': max_hour['RecTime__max'], 'attendence_counter': attaendence_counter, 'salary': salary})  # late
                        elif max_hour['RecTime__max'] < absent.time_out_evening:
                            attaendence_counter['early'] += 1
                            attendance.append({'date': i.RecDate.strftime('%Y-%m-%d'), 'color': 'bg-orange',
                                               'enter': min_hour['RecTime__min'],
                                               'out': max_hour['RecTime__max'], 'attendence_counter': attaendence_counter, 'salary': salary})  # early out
                        else:
                            attendance.append({'date': i.RecDate.strftime('%Y-%m-%d'), 'color': 'bg-success',
                                               'enter': min_hour['RecTime__min'],
                                               'out': max_hour['RecTime__max'], 'attendence_counter': attaendence_counter, 'salary': salary})  # in time
                old_date = i.RecDate

            else:
                # Declare Time (in and out)
                min_enter = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).order_by('RecTime').first()  # ex: 8:00
                min_out = list(AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).order_by('RecTime')[1:2])  # ex: 12:00
                max_enter = list(AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).order_by('-RecTime')[1:2])  # ex: 14:00
                max_out = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).order_by('RecTime').last()  # ex:18:00
                color_min = color_max = ''
                number_of_points_max = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).count()


                if number_of_points_max < 4 and i.RecDate != old_date:

                    attendance.append(
                        {'date': i.RecDate.strftime('%Y-%m-%d'), 'color_min': "bg-brown", 'color_max': "bg-brown",
                         'enter1': min_enter.RecTime,
                         'out1': min_out[0].RecTime,
                         'enter2': max_enter[0].RecTime,
                         'out2': max_out.RecTime, 'salary': salary})
                else:

                    if i.RecDate != old_date:
                        color_min = color_max = ''
                        stop_min = stop_max = False

                        if min_enter.RecTime > absent.time_enter_morning:
                            stop_min = True
                            attaendence_counter['late'] += 1
                            color_min = 'bg-yellow'  # late

                        if max_enter[0].RecTime > absent.time_enter_evening:
                            stop_max = True
                            attaendence_counter['late'] += 1
                            color_max = 'bg-yellow'  # late

                        if min_out[0].RecTime < absent.time_out_morning:
                            stop_min = True
                            attaendence_counter['early'] += 1
                            color_min = 'bg-orange'

                        if max_out.RecTime < absent.time_out_evening:
                            attaendence_counter['early'] += 1
                            stop_max = True
                            color_max = 'bg-orange'

                        if not stop_min:
                            color_min = 'bg-success'
                        if not stop_max:
                            color_max = 'bg-success'

                        # calculate number of working hours
                        #min
                        if min_enter.RecTime.hour <= absent.time_enter_morning.hour:
                            if min_out[0].RecTime.hour >= absent.time_out_morning.hour:
                                total_hours[0] += absent.time_out_morning.hour - absent.time_enter_morning.hour
                            else:
                                total_hours[0] += min_out[0].RecTime.hour - absent.time_enter_morning.hour
                                total_hours[1] += min_out[0].RecTime.minute

                            if min_enter.RecTime.minute > absent.time_enter_morning.minute:
                                total_hours[0] -= 1
                                total_hours[1] += 60 - min_enter.RecTime.minute
                        else:
                            if min_out[0].RecTime.hour >= absent.time_out_morning.hour:
                                total_hours[0] += absent.time_out_morning.hour - min_enter.RecTime.hour - 1
                                total_hours[1] += 60 - min_enter.RecTime.minute
                            else:
                                total_hours[0] += absent.time_out_morning.hour - min_enter.RecTime.hour - 1
                                total_hours[1] += 60 - min_enter.RecTime.minute
                        #max --------------
                        if max_enter[0].RecTime.hour <= absent.time_enter_evening.hour:
                            if max_out.RecTime.hour >= absent.time_out_evening.hour:
                                total_hours[2] += absent.time_out_evening.hour - absent.time_enter_evening.hour
                            else:
                                total_hours[2] += max_out.RecTime.hour - absent.time_enter_evening.hour
                                total_hours[3] += max_out.RecTime.minute

                            if max_enter[0].RecTime.minute > absent.time_enter_evening.minute:
                                total_hours[2] -= 1
                                total_hours[3] += 60 - max_enter[0].RecTime.minute
                        else:
                            if max_out.RecTime.hour >= absent.time_out_evening.hour:
                                total_hours[2] += absent.time_out_evening.hour - max_enter[0].RecTime.hour - 1
                                total_hours[3] += 60 - max_enter[0].RecTime.minute
                            else:
                                total_hours[2] += absent.time_out_evening.hour - max_enter[0].RecTime.hour - 1
                                total_hours[3] += 60 - max_enter[0].RecTime.minute
                        #end calculate of working hours

                        attendance.append(
                            {'date': i.RecDate.strftime('%Y-%m-%d'), 'color_min': color_min, 'color_max': color_max,
                             'enter1': min_enter.RecTime,
                             'out1': min_out[0].RecTime,
                             'enter2': max_enter[0].RecTime,
                             'out2': max_out.RecTime, 'salary': salary})

                old_date = i.RecDate;

        attendance = sorted(attendance, key=itemgetter('date'))  # sort the list by date

        if total_hours[1] > 59:
            total_hours[0] += total_hours[1] // 60
            total_hours[1] = int((total_hours[1] / 60 % 1) * 60)

        if not absent.permanence:
            if total_hours[3] > 59:
                total_hours[2] += total_hours[3] // 60
                total_hours[3] = int((total_hours[3] / 60 % 1) * 60)
            total_hours[0] += total_hours[2]
            total_hours[1] = total_hours[3]
            attaendence_counter['absent'] *= 2

        if total_hours[1] > 59:
            total_hours[0] += total_hours[1] // 60
            total_hours[1] = int((total_hours[1] / 60 % 1) * 60)
            
        attendance.append([total_hours, attaendence_counter]) 

        return attendance



class Profile(View):
    template_name = 'profile'

    def get(self, request, id):
        salary = Salary.objects.get(id=id)
        return render(request, 'pointage/employees/profile.html', {'salary': salary})

    def post(self, request, id):
        date1 = request.POST.get('date1')
        date2 = request.POST.get('date2')
        error = 'false'
        salary = Salary.objects.get(id=id)
        absent = Absent.objects.filter(group=salary.group_salary.id).first()

        if date1 > date2:
            error = 'la deuxième date devrait être plus grande que la premièredate'
            return render(request, 'pointage/employees/profile.html', {'salary': salary, 'error': error})
        if not date1 or not date2:
            error = 'Selectioner les deux dates'
            return render(request, 'pointage/employees/profile.html', {'salary': salary, 'error': error})

        # convert POST strings to dates
        date1 = datetime.datetime.strptime(request.POST.get('date1'), "%Y-%m-%d")
        date2 = datetime.datetime.strptime(request.POST.get('date2'), "%Y-%m-%d")
        # list of selected dates, ex from 22 to 29 from atdRecord
        count_compare = [(date1 + datetime.timedelta(days=i)).strftime('%Y-%m-%d') for i in
                         range(0, (date2 - date1).days + 1)]
        # list of pointages of X emp .strftime('%Y-%m-%d')
        pointages = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(RecDate__lte=date2).filter(
            RecDate__gte=date1)

        attaendence_counter = {'absent': 0, 'early': 0, 'late': 0}

        attendance = []
        exist_dates = []

        for pointage in pointages:
            if pointage.RecDate.strftime('%Y-%m-%d') in count_compare:
                exist_dates.append(pointage.RecDate.strftime('%Y-%m-%d'))

        for i in count_compare:
            if i not in exist_dates:
                check = Check()
                if not check.is_holiday(i):
                    if check.is_in_break(i, salary):
                        attendance.append({'date': i, 'color': 'bg-break'})  # Absent
                    else:
                        attaendence_counter['absent'] += 1
                        attendance.append({'date': i, 'color': 'bg-danger'})  # Absent
                else:
                    attendance.append({'date': i, 'color': 'bg-blue'})  # holiday

        old_date = ''
        if absent.permanence:
            total_hours = [0, 0]
        else:
            total_hours = [0, 0, 0 ,0]

        #start looping throug dates
        for i in pointages:
            number_of_points = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                RecDate=i.RecDate).count()

            if absent.permanence:

                min_hour = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).aggregate(Min('RecTime'))
                max_hour = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).aggregate(Max('RecTime'))

                if number_of_points < 2:
                    attendance.append(
                        {'date': i.RecDate.strftime('%Y-%m-%d'), 'color': 'bg-brown', 'enter': min_hour['RecTime__min'],
                         'out': min_hour['RecTime__min']})  # not complete
                else:
                    if i.RecDate != old_date:

                        # calculate number of working hours
                        if min_hour['RecTime__min'].hour <= absent.time_enter_morning.hour:
                            if max_hour['RecTime__max'].hour >= absent.time_out_evening.hour:
                                total_hours[0] += absent.time_out_evening.hour - absent.time_enter_morning.hour
                            else:
                                total_hours[0] += max_hour['RecTime__max'].hour - absent.time_enter_morning.hour
                                total_hours[1] += max_hour['RecTime__max'].minute

                            if min_hour['RecTime__min'].minute > absent.time_enter_morning.minute:
                                total_hours[0] -= 1
                                total_hours[1] += 60 - min_hour['RecTime__min'].minute
                        else:
                            if max_hour['RecTime__max'].hour >= absent.time_out_evening.hour:
                                total_hours[0] += absent.time_out_evening.hour - min_hour['RecTime__min'].hour - 1
                                total_hours[1] += 60 - min_hour['RecTime__min'].minute
                            else:
                                total_hours[0] += absent.time_out_evening.hour - min_hour['RecTime__min'].hour - 1
                                total_hours[1] += 60 - min_hour['RecTime__min'].minute
                        #end calculate of working hours

                        if min_hour['RecTime__min'] > absent.time_enter_morning:
                            attaendence_counter['late'] += 1
                            attendance.append({'date': i.RecDate.strftime('%Y-%m-%d'), 'color': 'bg-yellow',
                                               'enter': min_hour['RecTime__min'],
                                               'out': max_hour['RecTime__max'], 'attendence_counter': attaendence_counter})  # late
                        elif max_hour['RecTime__max'] < absent.time_out_evening:
                            attaendence_counter['early'] += 1
                            attendance.append({'date': i.RecDate.strftime('%Y-%m-%d'), 'color': 'bg-orange',
                                               'enter': min_hour['RecTime__min'],
                                               'out': max_hour['RecTime__max'], 'attendence_counter': attaendence_counter})  # early out
                        else:
                            attendance.append({'date': i.RecDate.strftime('%Y-%m-%d'), 'color': 'bg-success',
                                               'enter': min_hour['RecTime__min'],
                                               'out': max_hour['RecTime__max'], 'attendence_counter': attaendence_counter})  # in time
                old_date = i.RecDate

            else:
                # Declare Time (in and out)
                min_enter = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).order_by('RecTime').first()  # ex: 8:00
                min_out = list(AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).order_by('RecTime')[1:2])  # ex: 12:00
                max_enter = list(AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).order_by('-RecTime')[1:2])  # ex: 14:00
                max_out = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).order_by('RecTime').last()  # ex:18:00
                color_min = color_max = ''
                number_of_points_max = AtdRecord.objects.filter(CardNo_id=salary.id_salary_finger).filter(
                    RecDate=i.RecDate).count()


                if number_of_points_max < 4 and i.RecDate != old_date:
                    attendance.append(
                        {'date': i.RecDate.strftime('%Y-%m-%d'), 'color_min': "bg-brown", 'color_max': color_max,
                         'enter1': min_enter.RecTime,
                         'out1': min_out[0].RecTime,
                         'enter2': max_enter[0].RecTime,
                         'out2': max_out.RecTime})
                else:

                    if i.RecDate != old_date:
                        color_min = color_max = ''
                        stop_min = stop_max = False

                        if min_enter.RecTime > absent.time_enter_morning:
                            stop_min = True
                            attaendence_counter['late'] += 1
                            color_min = 'bg-yellow'  # late

                        if max_enter[0].RecTime > absent.time_enter_evening:
                            stop_max = True
                            attaendence_counter['late'] += 1
                            color_max = 'bg-yellow'  # late

                        if min_out[0].RecTime < absent.time_out_morning:
                            stop_min = True
                            attaendence_counter['early'] += 1
                            color_min = 'bg-orange'

                        if max_out.RecTime < absent.time_out_evening:
                            attaendence_counter['early'] += 1
                            stop_max = True
                            color_max = 'bg-orange'

                        if not stop_min:
                            color_min = 'bg-success'
                        if not stop_max:
                            color_max = 'bg-success'

                        # calculate number of working hours
                        #min
                        if min_enter.RecTime.hour <= absent.time_enter_morning.hour:
                            if min_out[0].RecTime.hour >= absent.time_out_morning.hour:
                                total_hours[0] += absent.time_out_morning.hour - absent.time_enter_morning.hour
                            else:
                                total_hours[0] += min_out[0].RecTime.hour - absent.time_enter_morning.hour
                                total_hours[1] += min_out[0].RecTime.minute

                            if min_enter.RecTime.minute > absent.time_enter_morning.minute:
                                total_hours[0] -= 1
                                total_hours[1] += 60 - min_enter.RecTime.minute
                        else:
                            if min_out[0].RecTime.hour >= absent.time_out_morning.hour:
                                total_hours[0] += absent.time_out_morning.hour - min_enter.RecTime.hour - 1
                                total_hours[1] += 60 - min_enter.RecTime.minute
                            else:
                                total_hours[0] += absent.time_out_morning.hour - min_enter.RecTime.hour - 1
                                total_hours[1] += 60 - min_enter.RecTime.minute
                        #max --------------
                        if max_enter[0].RecTime.hour <= absent.time_enter_evening.hour:
                            if max_out.RecTime.hour >= absent.time_out_evening.hour:
                                total_hours[2] += absent.time_out_evening.hour - absent.time_enter_evening.hour
                            else:
                                total_hours[2] += max_out.RecTime.hour - absent.time_enter_evening.hour
                                total_hours[3] += max_out.RecTime.minute

                            if max_enter[0].RecTime.minute > absent.time_enter_evening.minute:
                                total_hours[2] -= 1
                                total_hours[3] += 60 - max_enter[0].RecTime.minute
                        else:
                            if max_out.RecTime.hour >= absent.time_out_evening.hour:
                                total_hours[2] += absent.time_out_evening.hour - max_enter[0].RecTime.hour - 1
                                total_hours[3] += 60 - max_enter[0].RecTime.minute
                            else:
                                total_hours[2] += absent.time_out_evening.hour - max_enter[0].RecTime.hour - 1
                                total_hours[3] += 60 - max_enter[0].RecTime.minute
                        #end calculate of working hours

                        attendance.append(
                            {'date': i.RecDate.strftime('%Y-%m-%d'), 'color_min': color_min, 'color_max': color_max,
                             'enter1': min_enter.RecTime,
                             'out1': min_out[0].RecTime,
                             'enter2': max_enter[0].RecTime,
                             'out2': max_out.RecTime})

                old_date = i.RecDate;

        attendance = sorted(attendance, key=itemgetter('date'))  # sort the list by date

        if total_hours[1] > 59:
            total_hours[0] += total_hours[1] // 60
            total_hours[1] = int((total_hours[1] / 60 % 1) * 60)

        if not absent.permanence:
            if total_hours[3] > 59:
                total_hours[2] += total_hours[3] // 60
                total_hours[3] = int((total_hours[3] / 60 % 1) * 60)
            total_hours[0] += total_hours[2]
            total_hours[1] = total_hours[3]
            attaendence_counter['absent'] *= 2

        if total_hours[1] > 59:
            total_hours[0] += total_hours[1] // 60
            total_hours[1] = int((total_hours[1] / 60 % 1) * 60)

        return render(request, 'pointage/employees/profile.html',
                      {'salary': salary, 'date1': date1, 'date2': date2, 'error': error, 'attendance': attendance,
                       'absent': absent, 'total_hours': total_hours, 'attendence_counter': attaendence_counter})

class Report(View):
    template_name = "report"

    def get(self, request):
        groups = GroupSalaries.objects.all()
        return render(request, "pointage/employees/report.html", {'groups': groups})

    def post(self, request):
        date1 = request.POST.get('date1')
        date2 = request.POST.get('date2')
        groups = GroupSalaries.objects.all()
        group = request.POST.get('group')
        if group == "None":
            salaries = Salary.objects.all()
        else:
            salaries = Salary.objects.filter(group_salary=group)
        error = 'false'
        
        if date1 > date2:
            error = 'la deuxième date devrait être plus grande que la premièredate'
            return render(request, 'pointage/employees/report.html', {'error': error,'groups': groups})
        if not date1 or not date2:
            error = 'Selectioner les deux dates'
            return render(request, 'pointage/employees/report.html', {'error': error, 'groups': groups})

        # convert POST strings to dates
        date1 = datetime.datetime.strptime(request.POST.get('date1'), "%Y-%m-%d")
        date2 = datetime.datetime.strptime(request.POST.get('date2'), "%Y-%m-%d")
        # list of selected dates, ex from 22 to 29
        count_compare = [(date1 + datetime.timedelta(days=i)).strftime('%Y-%m-%d') for i in
                         range(0, (date2 - date1).days + 1)]
        pointages = []

        check = Check()
        for salary in salaries:
            pointages.append(check.pointages(count_compare, salary))
        groups = GroupSalaries.objects.all() # to show on the dropdown list
        return render(request, 'pointage/employees/report.html', {'pointages': pointages, "dates": count_compare, 'groups': groups})
