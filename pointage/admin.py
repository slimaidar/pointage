from django.contrib import admin

# Register your models here.
from pointage.models import Salary, GroupSalaries, AtdRecord, Absent, Holidays, Break

admin.site.register(Salary)
admin.site.register(GroupSalaries)
admin.site.register(AtdRecord)
admin.site.register(Absent)
admin.site.register(Holidays)
admin.site.register(Break)
