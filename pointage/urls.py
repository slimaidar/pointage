from django.urls import path
from .views import Employees, Profile, Report

urlpatterns = [
    path('employees', Employees.as_view(), name="employees-home"),
    path('profile/<int:id>', Profile.as_view(), name="profile"),
    path('reports', Report.as_view(), name="reports"),
]